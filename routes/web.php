<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/zoeken', 'CarController@index')->name('search')->middleware('auth');
Route::post('/zoeken', 'CarController@searchCar')->name('searchCar')->middleware('auth');
Route::get('/auto/{license}', 'CarController@getCar')->name('getCar')->middleware('auth');
Route::get('/auto', 'CarController@getCar')->name('getCarEmpty')->middleware('auth');
Route::post('/auto/comment', 'CarController@postComment')->name('post.comment')->middleware('auth');

// Favorites
Route::post('/favorite', 'FavoriteController@save')->name('saveFavorite')->middleware('auth');
