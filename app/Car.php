<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp;

class Car extends Model
{

    /**
     * Haalt gegevens op via Kenteken Wiki API (Middlemen naar RDW)
     * @param $license
     * @return mixed
     */
    public static function getCar($license) {
        $client = new GuzzleHttp\Client();

        $res = $client->request("GET","https://api.kenteken-wiki.nl/v1/car/${license}/info", [
            'headers' => [
                'User-Agent' => 'Kenteken-Wiki App v',
            ]
        ]);

        return json_decode($res->getBody()->getContents());
    }
}
