<?php

namespace App\Http\Controllers;

use App\Car;
use App\Comments;
use App\Favorites;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CarController extends Controller
{
    /**
     * Laat view zien van zoekpagina
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('search/index');
    }

    /**
     * Stuurt uitkomst van form door naar getCar functie.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function searchCar(Request $request)
    {
        $rawLicense = strtoupper($request->input('carLicense'));
        $license = str_replace('-', '', $rawLicense);
        return redirect()->route('getCar', [$license]);
    }

    /**
     * Haalt auto's op en toont stuurt informatie door view met hulp van de Car::getCar functie
     * @param $license
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getCar($license = null)
    {
        if (!$license) {
            return redirect()->route('search')->with('error', "Je hebt geen kenteken ingevoerd!");
        }
        $favorite = Favorites::where(['license' => $license, 'user_id' => Auth::user()->id])->exists();
        $car = Car::getCar($license);
        $comments = Comments::where('license', $license)
            ->orderBy('created_at', 'desc')
            ->get();
        if ($car->error) {
            return redirect()->route('search')->with('error', "Geen auto gevonden met het kenteken: ${license}!");
        }
        return view('search/info', ["info" => $car->info, "comments" => $comments, "favorite" => $favorite]);
    }

    /**
     * Regelt het request bij het plaatsen van een opmerking
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postComment(Request $request)
    {
        $comment = new Comments();
        $comment->license = $request->input('license');
        $comment->comment = $request->input('post-comment');
        $comment->user = Auth::user()->name;
        $comment->save();
        return redirect()->route('getCar', [$request->input('license')])->with('success', "Opmerking toegevoegd!");
    }
}
