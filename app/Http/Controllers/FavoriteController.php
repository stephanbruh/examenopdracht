<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favorites;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public function save(Request $request) {
        $exist = $request->input('exist');
        $userID = $request->input('user_id');
        $license = $request->input('license');
        $model = $request->input('model');
        $color = $request->input('color');

        if ($exist) {
            Favorites::where(['license' => $license, 'user_id' => Auth::user()->id])->delete();
            return redirect()->route('getCar', [$request->input('license')])->with('success', "Verwijderd van favorieten!");
        }

        $favorite = new Favorites();
        $favorite->user_id = $userID;
        $favorite->license = $license;
        $favorite->model = $model;
        $favorite->color = $color;
        $favorite->save();

        return redirect()->route('getCar', [$request->input('license')])->with('success', "Toegevoegd aan favorieten!");
    }
}
