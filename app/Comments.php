<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Te7aHoudini\LaravelTrix\Traits\HasTrixRichText;

class Comments extends Model
{
    protected $table = 'comments';

    protected $fillable = [
        'license', 'comment', 'user'
    ];
}
