<?php
/**
 * Zet alles om zodat begint met hoofdletter
 * @param $string
 * @return string
 */
function translate($string) {
    $string = strtolower($string);
    return ucfirst($string);
}

/**
 * Zet kW om naar PK's
 * @param $brandstof
 * @return string
 */
function getPK($brandstof) {
    if (isset($brandstof[0]->nettomaximumvermogen)){
        $kw = round($brandstof[0]->nettomaximumvermogen);
        $pk = round($brandstof[0]->nettomaximumvermogen * 1.4);
        return "${pk} PK (${kw} kW)";
    } elseif (isset($brandstof[1]->nettomaximumvermogen)) {
        $kw = round($brandstof[1]->nettomaximumvermogen);
        $pk = round($brandstof[1]->nettomaximumvermogen * 1.4);
        return "${pk} PK (${kw} kW)";
    }else {
        return "Onbekend";
    }
}

/**
 * Vergelijkt de datum eerste afgifte NL en eerste toelating en bepaald of auto geimporteerd is.
 * @param $info
 * @return string
 */
function imported($info) {
    $afgiftenl = strtotime($info->datum_eerste_afgifte_nederland);
    $toelating = strtotime($info->datum_eerste_toelating);

    return $toelating >= $afgiftenl ? "Nee" : "Ja";
}
