@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ translate($info->merk) }} {{ translate($info->handelsbenaming) }} @include('search/favorite')</div>

                    <div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif
                        <br>
                        <h4>Algemeen</h4>
                        <table class="table table-striped info-table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Kenteken</th>
                                <td>{{ $info->kenteken ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Merk</th>
                                <td>{{ translate($info->merk ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Handelsbenaming</th>
                                <td>{{ translate($info->handelsbenaming ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Type</th>
                                <td>{{ $info->type ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th scope="row">PK</th>
                                <td>{{ getPK($info->brandstof ?? '-')  }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Catalogusprijs</th>
                                <td> €{{ $info->catalogusprijs ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th scope="row">BPM</th>
                                <td> €{{ $info->bruto_bpm ?? '-' }}</td>
                            </tr>
                            </tbody>
                        </table>

                        <br>
                        <h4>Uiterlijk</h4>
                        <table class="table table-striped info-table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Soort</th>
                                <td>{{ translate($info->voertuigsoort ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Inrichting</th>
                                <td>{{ translate($info->inrichting ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Kleur</th>
                                <td>{{ translate($info->eerste_kleur ?? '-') }}</td>
                            </tr>
                            </tbody>
                        </table>

                        <br>
                        <h4>Historie</h4>
                        <table class="table table-striped info-table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Datum eerste toelating</th>
                                <td>{{ translate($info->datum_eerste_toelating ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Afgifte Nederland</th>
                                <td>{{ translate($info->datum_eerste_afgifte_nederland ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Datum laatste tenaamstelling</th>
                                <td>{{ translate($info->datum_tenaamstelling ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">APK Vervaldatum</th>
                                <td>{{ translate($info->vervaldatum_apk ?? '-') }}</td>
                            </tr>
                            </tbody>
                        </table>

                        <br>
                        <h4>Status</h4>
                        <table class="table table-striped info-table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Terugroepactie</th>
                                <td>{{ translate($info->datum_eerste_toelating ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">WAM Verzekerd</th>
                                <td>{{ translate($info->wam_verzekerd ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Taxi</th>
                                <td>{{ translate($info->taxi_indicator ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Geimporteerd</th>
                                <td>{{ imported($info) }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Wachten Op Keuren</th>
                                <td>{{ translate($info->wacht_op_keuren ?? '-') }}</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <h4>Motorisch</h4>
                        <table class="table table-striped info-table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Cilinders</th>
                                <td>{{ translate($info->aantal_cilinders ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Cilinderinhoud</th>
                                <td>{{ translate($info->cilinderinhoud ?? '-') }}cm³</td>
                            </tr>
                            <tr>
                                <th scope="row">Zuinigheidslabel</th>
                                <td>{{ translate($info->zuinigheidslabel ?? '-') }}</td>
                            </tr>
                            </tbody>
                        </table>

                        @foreach($info->brandstof as $brandstof)
                            <br>
                            <h4>Brandstof {{ $brandstof->brandstof_omschrijving }}</h4>
                            <table class="table table-striped info-table">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Verbruik Snelweg</th>
                                    <td>{{ translate($brandstof->brandstofverbruik_buiten ?? '-') }}/100km</td>
                                </tr>
                                <tr>
                                    <th scope="row">Verbruik Stad</th>
                                    <td>{{ translate($brandstof->brandstofverbruik_stad ?? '-') }}/100km</td>
                                </tr>
                                <tr>
                                    <th scope="row">Verbruik Gecombineerd</th>
                                    <td>{{ translate($brandstof->brandstofverbruik_gecombineerd ?? '-') }}/100km</td>
                                </tr>
                                <tr>
                                    <th scope="row">Co2 Uitstoot</th>
                                    <td>{{ translate($brandstof->co2_uitstoot_gewogen ?? '-') }}/g/km</td>
                                </tr>
                                <tr>
                                    <th scope="row">Geluidsniveau rijdend</th>
                                    <td>{{ translate($brandstof->geluidsniveau_rijdend ?? '-') }} dB</td>
                                </tr>
                                <tr>
                                    <th scope="row">Geluidsniveau stationair</th>
                                    <td>{{ translate($brandstof->geluidsniveau_stationair ?? '-') }} dB</td>
                                </tr>
                                </tbody>
                            </table>
                        @endforeach

                        <br>
                        <h4>Eigenschappen</h4>
                        <table class="table table-striped info-table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Aantal zitplaatsen</th>
                                <td>{{ translate($info->aantal_zitplaatsen ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Aantal wielen</th>
                                <td>{{ translate($info->aantal_wielen ?? '-') }}cm³</td>
                            </tr>
                            <tr>
                                <th scope="row">Aantal deuren</th>
                                <td>{{ translate($info->aantal_deuren ?? '-') }}</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <h4>Carroserie</h4>
                        <table class="table table-striped info-table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Carroserie-nummer</th>
                                <td>{{ translate($info->carrosserie_volgnummer ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Categorie</th>
                                <td>{{ translate($info->europese_voertuigcategorie ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Type</th>
                                <td>{{ translate($info->carrosserietype ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Type omschrijving</th>
                                <td>{{ translate($info->type_carrosserie_europese_omschrijving ?? '-') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Plaats chassisnummer</th>
                                <td>{{ translate($info->plaats_chassisnummer ?? '-') }}</td>
                            </tr>
                            </tbody>
                        </table>
                            <br>
                        <h4>Massa</h4>
                        <table class="table table-striped info-table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Leeg gewicht</th>
                                <td>{{ translate($info->massa_ledig_voertuig ?? '-') }}kg</td>
                            </tr>
                            <tr>
                                <th scope="row">Rijklaar gewicht</th>
                                <td>{{ translate($info->massa_rijklaar ?? '-') }}kg</td>
                            </tr>
                            <tr>
                                <th scope="row">Wettelijk limiet massa</th>
                                <td>{{ translate($info->toegestane_maximum_massa_voertuig ?? '-') }}kg</td>
                            </tr>
                            <tr>
                                <th scope="row">Maximum massa</th>
                                <td>{{ translate($info->type_carrosserie_europese_omschrijving ?? '-') }}kg</td>
                            </tr>
                            <tr>
                                <th scope="row">Technische limiet massa</th>
                                <td>{{ translate($info->toegestane_maximum_massa_voertuig ?? '-') }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        Opmerkingen
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('post.comment') }}">
                            <div class="form-group">
                                <input hidden id="license" name="license" value="{{ $info->kenteken }}">
                                <textarea id="post-comment" name="post-comment"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Plaatsen</button>
                            {{csrf_field()}}
                        </form>
                        <br>
                        @foreach($comments as $comment)
                            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" style="opacity: 1; box-shadow: none">
                                <div class="toast-header">
                                    <strong class="mr-auto">{{ $comment->user }}</strong>
                                    <small>{{ $comment->created_at->diffForHumans() }}</small>
                                </div>
                                <div class="toast-body">
                                    {!! $comment->comment  !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
@endsection
