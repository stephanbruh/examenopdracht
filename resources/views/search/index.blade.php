@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Zoeken</div>

                    <div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif

                        <p>Hier kunt u de informatie van een specifiek voertuig opzoeken op basis van kenteken.</p>
                            <form method="POST" action="{{ route('searchCar') }}">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="carLicense" name="carLicense" aria-describedby="emailHelp" placeholder="Kenteken" required>
                                    <small id="emailHelp" class="form-text text-muted">De streepjes zijn niet verplicht.</small>
                                </div>
                                <button type="submit" class="btn btn-primary">Zoeken</button>

                                {{csrf_field()}}
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
