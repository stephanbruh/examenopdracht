@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Welkom in het portaal!

                    </div>
                </div>

                <div class="card" style="margin-top: 25px">
                    <div class="card-header">Zoeken</div>

                    <div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif

                        <p>Hier kunt u de informatie van een specifiek voertuig opzoeken op basis van kenteken.</p>
                        <form method="POST" action="{{ route('searchCar') }}">
                            <div class="form-group">
                                <input type="text" class="form-control" id="carLicense" name="carLicense"
                                       aria-describedby="emailHelp" placeholder="Kenteken" required>
                                <small id="emailHelp" class="form-text text-muted">De streepjes zijn niet
                                    verplicht.</small>
                            </div>
                            <button type="submit" class="btn btn-primary">Zoeken</button>

                            {{csrf_field()}}
                        </form>
                    </div>
                </div>

                <div class="card favorites">
                    <div class="card-header">Favorieten</div>

                    <div class="card-body">
                        @if (!count($favorites))
                            Je hebt nog geen favorieten. Druk op de ster in de info pagina om deze opteslaan als favoriet.
                        @endif
                        @foreach($favorites as $favorite)
                            <div class="favorite-block">
                                <span class="license">{{ $favorite->license }}</span>
                                <span class="model">{{ $favorite->model }}</span>
                                <span class="color">{{ $favorite->color }}</span>
                                <a href="/auto/{{ $favorite->license }}">Bekijken</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
