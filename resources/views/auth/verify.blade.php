@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Verifeer je emailadres</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            Een nieuwe link is gestuurd naar je emailadres.
                        </div>
                    @endif

                    Voordat je vergaat, check je email voor een verificatie link.
                    Geen email ontvangen?,
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">Klik hier voor een nieuwe link</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
